﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DemoApp.Data;
using DemoApp.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DemoApp.Controllers
{
    // [Route("api/[controller]")]
    [Produces("application/json")]
    [Route("vremeapi")]
    [ApiController]
    public class VremeAPIController : ControllerBase
    {

         DataContext context = new DataContext();
            
        
            
        // GET: api/<VremeAPIController>
        [HttpGet("/ListCities")]
        public List <City> ListCities()
        {
          //  context.Cities.Add(mesto1);
           // context.SaveChanges();

            var cities = from city in context.Cities
                         select city;

            List<City> mesta = new List<City>();
            foreach(City c in cities)
            {
                mesta.Add(c);
            }

            return mesta;
        }

      

        
        [HttpPost("/CreateCity")]    
        public void CreateCity(City city)
        {   
            //ce je mesto ze v bazi isInDB ne bo null
            var isInDB = context.Cities.Where(c => c.Name == city.Name).FirstOrDefault();
            if(isInDB is null)
            {
                //Doda objekt iz parametra city v bazo
                context.Cities.Add(city);
                context.SaveChanges();
            }
            else
            {
                Console.WriteLine("Mesto je ze v bazi");
                
            }

        }
        [HttpPost("/CreateCountries")]
        public void CreateCountries(Country [] countries)
        {
            //loop skoz vse drzave iz parametra
            foreach(Country c in countries)
            {
                //preveri ali je drzava v bazi
                var isInDB = context.Countries.Where(drzava => drzava.Name == c.Name).FirstOrDefault();
               if(isInDB is null)
                {
                    //ce ni, jo doda
                    context.Countries.Add(c);
                    context.SaveChanges();
                }
                else
                {
                    Console.WriteLine("Drzava je ze v bazi");
                }
            }

        }     
    }
}
