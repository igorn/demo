﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Models
{
    public class City
    {   
        [Key]
        public int CityId { get; set; }
        public int CountryId { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public int TemperatureInCelsius { get; set; }
        public DateTime Date { get; set; }

        public DateTime TimeStamp { get; set; }
    }
}
