﻿using DemoApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoApp.Data
{
    public class DataContext :DbContext
    {
        public DbSet <Country> Countries { get; set; }
        public DbSet <City>    Cities { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            //slaba praksa da se povezava hardcode-a
            options.UseSqlServer("server=(localdb)\\MSSQLLocalDB;database=vremeDB");
        }
    }
}
